const gulp = require('gulp')
const electron = require('electron-connect').server.create()
const env = require('gulp-env')
const config = require('@ionic/app-scripts/dist/util/config')
const ionic = require('@ionic/app-scripts')
const replace = require('replace')
const replaceFiles = ['./www/build/main.js']

gulp.task('electron-live', function () {
  env.set({
    NODE_ENV: 'development'
  })
  // Start browser process
  electron.start()

  gulp.watch('src/app.js', electron.restart)

  // Reload renderer process
  gulp.watch([
    'www/js/app.js',
    'www/**/*.html',
    'www/**/*.css',
    'www/**/*.js'], electron.reload)
})

gulp.task('dev', function () {
  ionic.watch(config.generateContext())
  .then(function () {
    gulp.start('electron-live')
  })
  .catch(function (err) {
    console.log('Error starting watch: ', err)
  })
})

gulp.task('addProxy', function() {
  return replace({
    regex: "https://atdso-dev.bettywebblocks.com/api",
    replacement: "/api",
    paths: replaceFiles,
    recursive: false,
    silent: false,
  })
})

gulp.task('removeProxy', function() {
  return replace({
    regex: "/api",
    replacement: "https://atdso-dev.bettywebblocks.com/api",
    paths: replaceFiles,
    recursive: false,
    silent: false,
  })
})

gulp.task('addProdUrl', function() {
  return replace({
    regex: "/api",
    replacement: "https://atdso-dev.bettywebblocks.com/api",
    paths: replaceFiles,
    recursive: false,
    silent: false,
  })
})