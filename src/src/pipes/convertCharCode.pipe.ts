import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'convertCharCode'
})

export class ConvertCharCode implements PipeTransform {
  transform(value: any): any {
    let newVal = value.replace(/&#(.*);/g, (m, g) => {
        return String.fromCharCode(g);
    })
    value = newVal
    return value
  }
}