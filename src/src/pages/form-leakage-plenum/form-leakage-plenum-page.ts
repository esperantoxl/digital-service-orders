import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';


@Component({
  selector: 'form-leakage-plenum-page',
  templateUrl: 'form-leakage-plenum-page.html'
})

export class FormLeakagePlenumPage {
  orderline: any
  formStructure: any

  constructor(
    private navParams: NavParams,
    public storage: Storage,
    private navCtrl: NavController,
    public headerServiceProvider: HeaderServiceProvider,
    public validationFormServiceProvider: ValidationFormServiceProvider,
    private ordersServiceProvider: OrdersServiceProvider
  ) {
    this.orderline = this.navParams.get('orderline')
    this.formStructure = this.navParams.get('formStructure')
  }
  
  submitFormHandler(formData) {
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.validationFormServiceProvider.postLeakagePlenum(token, this.orderline.id, formData).subscribe(
        data => {
          for (var key in data) {
            if (data.hasOwnProperty(key)) { 
              var prop = data[key];
              this.orderline.leakage_plenums[0][key] = prop
            }
          }
          this.headerServiceProvider.loading = false
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        },
        err => {
          this.headerServiceProvider.loading = false
          for (var key in formData) {
            if (formData.hasOwnProperty(key)) { 
              var prop = formData[key];
              this.orderline.leakage_plenums[0][key] = prop
            }
          }
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        }
      )
    })
  }

  navBack() {
    this.navCtrl.pop()
  }
}