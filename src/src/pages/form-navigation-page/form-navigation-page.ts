import { FormCommentsPage } from './../form-comments-page/form-comments-page';
import { FormWorkspacePage } from './../form-workspace-page/form-workspace-page';
import { FormLeakagePlenumPage } from './../form-leakage-plenum/form-leakage-plenum-page';
import { FormLeakageHepaPage } from './../form-leakage-hepa/form-leakage-hepa-page';
import { FormGeneralPage } from './../form-general-page/form-general-page';
import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { FormComponentsPage } from './../form-components-page/form-components-page';
import { FormAirDownflowPage } from './../form-air-downflow-page/form-air-downflow-page';
import { EquipmentPage } from './../equipment/equipment';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { FormAirInflowPage } from './../form-air-inflow-page/form-air-inflow-page';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
    selector: 'form-navigation-page',
    templateUrl: 'form-navigation-page.html'
})

export class FormNavigationPage {
    orderline: any
    showEquipment: boolean = true
    order: any

    constructor(
        private navParams: NavParams,
        public validationFormServiceProvider: ValidationFormServiceProvider,
        public storage: Storage,
        private navCtrl: NavController,
        public headerServiceProvider: HeaderServiceProvider,
        public ordersServiceProvider: OrdersServiceProvider
    ) {
        this.orderline = this.navParams.get('orderline')
        this.order = navParams.get('order')
        this.initForms()
        this.orderline.testvalue = ''
    }

    initForms () {
        // components form
        if (!this.orderline.component_controls[0]) {
            const componentProps = this.orderline.form_navigation_structure.components.properties
            this.orderline.component_controls[0] = {}

            for (let i = 0; i < componentProps.length; i++) {
                let property = componentProps[i];
                this.orderline.component_controls[0][property.property] = ''
            } 
        }

        // general form
        if (!this.orderline.general_measurements[0]) {
            const generalProps = this.orderline.form_navigation_structure.general_measurment.properties
            this.orderline.general_measurements[0] = {}

            for (let i = 0; i < generalProps.length; i++) {
                let property = generalProps[i];
                this.orderline.general_measurements[0][property.property] = ''
            } 
        }
        
        // workspace form
        if (!this.orderline.classification_workspaces[0]) {
            this.orderline.classification_workspaces[0] = {}

            this.orderline.classification_workspaces[0]['left_03'] = ''
            this.orderline.classification_workspaces[0]['right_03'] = ''
            this.orderline.classification_workspaces[0]['left_05'] = ''
            this.orderline.classification_workspaces[0]['right_05'] = ''
        }

        // plenum form
        if (!this.orderline.leakage_plenums[0]) {
            const plenumProps = this.orderline.form_navigation_structure.leakage_plenum.properties
            this.orderline.leakage_plenums[0] = {}

            for (let i = 0; i < plenumProps.length; i++) {
                let property = plenumProps[i];
                this.orderline.leakage_plenums[0][property.property] = ''
            } 
        }
        
        // hepa form
        if (!this.orderline.leakage_hepas[0]) {
            const method = this.orderline.protocol.hepa_leakage_method
            if (method === 'Particle Count') {
                const hepaPropsA = this.orderline.form_navigation_structure.leakage_hepa.particle_count.downflow
                const hepaPropsB = this.orderline.form_navigation_structure.leakage_hepa.particle_count.exaust
                const hepaPropsC = this.orderline.form_navigation_structure.leakage_hepa.pressure

                this.orderline.leakage_hepas[0] = {}

                for (let i = 0; i < hepaPropsA.length; i++) {
                    let property = hepaPropsA[i];
                    this.orderline.leakage_hepas[0][property.property] = ''
                } 
                for (let i = 0; i < hepaPropsB.length; i++) {
                    let property = hepaPropsB[i];
                    this.orderline.leakage_hepas[0][property.property] = ''
                } 
                for (let i = 0; i < hepaPropsC.length; i++) {
                    let property = hepaPropsC[i];
                    this.orderline.leakage_hepas[0][property.property] = ''
                } 
            } else if (method === 'Aerosol') {
                const hepaPropsA = this.orderline.form_navigation_structure.leakage_hepa.aerosol
                const hepaPropsB = this.orderline.form_navigation_structure.leakage_hepa.pressure
                    
                this.orderline.leakage_hepas[0] = {}

                for (let i = 0; i < hepaPropsA.length; i++) {
                    let property = hepaPropsA[i];
                    this.orderline.leakage_hepas[0][property.property] = ''
                } 
                for (let i = 0; i < hepaPropsB.length; i++) {
                    let property = hepaPropsB[i];
                    this.orderline.leakage_hepas[0][property.property] = ''
                } 
            }
        }
        
        // Comment form
        if (!this.orderline.communication_logs[0]) {
            this.orderline.communication_logs[0] = {}
            this.orderline.communication_logs[0]['comment'] = ''
        }
    }

    navigateToAirspeedInflowValidation() {
        this.navCtrl.push(FormAirInflowPage, {orderline: this.orderline, formStructure: this.orderline.form_navigation_structure.airspeed.inflow_structure})
    }

    navigateToAirspeedDownflowValidation() {
        this.navCtrl.push(FormAirDownflowPage, {orderline: this.orderline, formStructure: this.orderline.form_navigation_structure.airspeed.downflow_structure})
    }

    navigateToFormComponentsPage() {
        this.navCtrl.push(FormComponentsPage, {orderline: this.orderline, formStructure: this.orderline.form_navigation_structure.components})
    }

    navigateToEquipment() {
        this.storage.get('order').then(order => {
            this.navCtrl.push(EquipmentPage, {selected_orderline: this.orderline, selected_order: order})
        })
    }

    navigateToGeneral() {
        this.navCtrl.push(FormGeneralPage, {orderline: this.orderline, formStructure: this.orderline.form_navigation_structure.general_measurment})
    }
    
    navigateToComments() {
        this.navCtrl.push(FormCommentsPage, {orderline: this.orderline, formStructure: {}})
    }

    navigateToLeakageHepa() {
        this.navCtrl.push(FormLeakageHepaPage, {orderline: this.orderline, formStructure: this.orderline.form_navigation_structure.leakage_hepa})
    }

    navigateToLeakagePlenum() {
        this.navCtrl.push(FormLeakagePlenumPage, {orderline: this.orderline, formStructure: this.orderline.form_navigation_structure.leakage_plenum})
    }

    navigateToWorkspace() {
        this.navCtrl.push(FormWorkspacePage, {orderline: this.orderline, formStructure: this.orderline.form_navigation_structure.leakage_plenum})
    }

    closeOrderline() {
        this.headerServiceProvider.loading = true
        this.storage.get('token').then(token=>{
            this.ordersServiceProvider.closeOrderline(this.orderline.id, token).subscribe(
                data=> {
                    this.orderline.status = 'Done'
                    this.navCtrl.pop()
                    this.headerServiceProvider.loading = false
                },
                err => {

                }
            )
        })
    }
    
    navBack() {
        this.navCtrl.pop()
    }
}