import { OrdersPage } from './../orders/orders'
import { Component } from '@angular/core'
import { NavController } from 'ionic-angular'
import { UserServiceProvider } from "../../providers/user-service/user-service"
import { Storage } from "@ionic/storage"

declare const electron
declare const path

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {
  authUser: any
  error: any
  pageIsLoading: any = false

  constructor(
    public navCtrl: NavController, 
    public userServiceProvider: UserServiceProvider, 
    public storage: Storage
  ) {
    this.isLoggedIn()
  }

  authenticate(form) {
    this.pageIsLoading = true
    this.userServiceProvider.login(form).subscribe(
        data => {
          this.error = null
          this.authUser = data.user 
          this.authSuccess(data.id_token) 
        },
        err => {
          this.authError(err._body)
        }
      )
  }
  
  authSuccess(token) {
    this.storage.set('token', token)
    this.navCtrl.setRoot(OrdersPage, {}, {animate: true, direction: 'forward'})
    this.pageIsLoading = false
  }

  authError(err) {
    this.error = err
    this.pageIsLoading = false
  }

  isLoggedIn() {
    this.storage.get('token').then(token => {
      if (token) {
        this.pageIsLoading = true
        this.userServiceProvider.isLoggedIn(token).subscribe(
          data => {
            if (data.user) {
              this.navCtrl.setRoot(OrdersPage, {}, {animate: true, direction: 'forward'});
            }
            this.pageIsLoading = false
          },
          err => {
            this.pageIsLoading = false
            this.navCtrl.setRoot(OrdersPage, {}, {animate: true, direction: 'forward'});
          }
        )
      } else {
        this.pageIsLoading = false
      }
    }).catch(e => {
      this.pageIsLoading = false
    })
  }

}
