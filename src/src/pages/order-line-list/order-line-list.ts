import { OrderCommentsPage } from './../order-comment-page/order-comments-page';
import { CompleteOrderSignaturePage } from './../complete-order-signature/complete-order-signature';
import { OrdersServiceProvider } from './../../providers/orders-service/orders-service'
import { FormNavigationPage } from './../form-navigation-page/form-navigation-page'
import { EquipmentPage } from './../equipment/equipment'
import { Storage } from '@ionic/storage'
import { NavController, NavParams, ModalController } from 'ionic-angular'
import { HeaderServiceProvider } from './../../providers/header-service/header-service'
import { Component } from '@angular/core'

@Component ({
  selector: 'page-orders',
  templateUrl: 'order-line-list.html'
})

export class OrderLineList {
  order: any
  orderlines: any = []
  completed_orderlines: any = []
  pageTitle: string

  constructor(
    public navCtrl: NavController, 
    public storage: Storage,
    public headerServiceProvider: HeaderServiceProvider,
    private navParams: NavParams,
    public ordersServiceProvider: OrdersServiceProvider,
    public modalController: ModalController
  ) {
      this.order = navParams.get('order')
      this.pageTitle = this.order.order_number
      for (let i = 0; i < this.order.orderlines.length; i++) {
        let orderline = this.order.orderlines[i];
        if (!orderline.assignee) {
          orderline.assignee = {
            full_name: ''
          }
        }
        if (orderline.status === 'Done') {
          this.completed_orderlines.push(orderline)
        } else if (orderline.status != 'On-Hold') { 
          this.orderlines.push(orderline)
        }
      }
  }

  ionViewDidEnter () {
    this.orderlines = []
    this.completed_orderlines = []
    this.pageTitle = this.order.order_number
    for (let i = 0; i < this.order.orderlines.length; i++) {
      let orderline = this.order.orderlines[i];
      if (orderline.status === 'Done') {
        this.completed_orderlines.push(orderline)
      } else if (orderline.status != 'On-Hold') { 
        this.orderlines.push(orderline)
      }
    }
  }

  navBack() {
      this.navCtrl.pop()
  }

  openSignatureModel(){
    setTimeout(() => {
      let modal = this.modalController.create(CompleteOrderSignaturePage, {order: this.order});
      modal.present();
    }, 300);
  }

  openOrderComment() {
    this.navCtrl.push(OrderCommentsPage, {order: this.order})
  }

  selectOrderline(orderline, order) {
    this.navCtrl.push(FormNavigationPage, {orderline: orderline, order: this.order})
    this.storage.get('token').then(token => {
      this.ordersServiceProvider.setOrderlineStatus(orderline.id, token, 'In progress').subscribe(
        data => {
          let orderlineIndex = 0
          for (var i = 0; i < this.orderlines.length; i++) {
            const selectedOrderline = this.orderlines[i];
            if(selectedOrderline.id === orderline.id) {
              orderlineIndex = i
            } 
          }
          this.orderlines[orderlineIndex].assignee = {
            full_name: data.full_name
          }
        },
        err => {
          let orderlineIndex = 0
          for (var i = 0; i < this.orderlines.length; i++) {
            const selectedOrderline = this.orderlines[i];
            if(selectedOrderline.id === orderline.id) {
              orderlineIndex = i
            } 
          }
          if (this.headerServiceProvider.offline) {
            this.orderlines[orderlineIndex].changedOffline = true
            this.ordersServiceProvider.saveOrderlineToStorage(this.orderlines[orderlineIndex])
          }
        }
      )
    })
  }

  startOrder(orderID) {
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.ordersServiceProvider.setOrderStatus(orderID, token, "In progress").subscribe(
        data => {
          this.order.status = 'In progress'
          this.headerServiceProvider.loading = false
        }
      )
    });
  }
}
