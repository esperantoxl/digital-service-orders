import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
    selector: 'form-air-downflow-page',
    templateUrl: 'form-air-downflow-page.html'
})

export class FormAirDownflowPage {
    orderline: any
    navDefinition: any
    formStructure: any
    measurement_number: any = 1
    measurement_possible: any = true
    criteria: any = {
        operator: '>=',
        value: '0.4'
    }
    value: any = {
    }
    avgFlowSpeed: number = 0
    minInvVal: number
    maxInvVal: number
    averageStandbySpeed: number = 0

    constructor(
        private navParams: NavParams,
        public storage: Storage,
        private navCtrl: NavController,
        public validationFormServiceProvider: ValidationFormServiceProvider,
        public headerServiceProvider: HeaderServiceProvider,
        public ordersServiceProvider: OrdersServiceProvider
    ) {
        this.orderline = this.navParams.get('orderline')
        this.formStructure = this.navParams.get('formStructure')
        if (this.orderline.airspeed_downflows.length == 1) {
            this.measurement_number = 2
            this.measurement_possible = true
        } else if (this.orderline.airspeed_downflows.length > 1){
            this.measurement_possible = false
        }
        this.calcMinMaxVal()
    }

    calcAverage(value) {
        let sumary: number = 0
        let numOfKeys = 0
        for (var key in value) {
            if (value.hasOwnProperty(key)) {
                sumary += parseFloat(value[key])
                numOfKeys++;
            }
        }
        let avg = sumary/numOfKeys || 0
        this.avgFlowSpeed = Math.round(avg * 100) / 100
        this.calcMinMaxVal()
    }

    calcMinMaxVal () {
        let maxVal = (this.orderline.protocol_criteria[0].df_tolerance * this.avgFlowSpeed) + this.avgFlowSpeed
        this.maxInvVal = Math.round(maxVal * 100) / 100
        let minVal = this.avgFlowSpeed - (this.orderline.protocol_criteria[0].df_tolerance * this.avgFlowSpeed)
        this.minInvVal = Math.round(minVal * 100) / 100
    }

    submitFormHandler(formValue){
        this.headerServiceProvider.loading = true
        formValue['mean_value'] = this.avgFlowSpeed
        this.storage.get('token').then(token => {
            this.validationFormServiceProvider.postAirDownflowForm(token, this.orderline.id, formValue, this.measurement_number).subscribe(
                data => {
                    this.headerServiceProvider.loading = false
                    this.orderline.airspeed_downflows.push({
                        measurement_number: parseFloat(this.measurement_number),
                        a1: parseFloat(formValue.a1),
                        a2: parseFloat(formValue.a2),
                        a3: parseFloat(formValue.a3),
                        a4: parseFloat(formValue.a4),
                        a5: parseFloat(formValue.a5),
                        a6: parseFloat(formValue.a6),
                        b1: parseFloat(formValue.b1),
                        b2: parseFloat(formValue.b2),
                        b3: parseFloat(formValue.b3),
                        b4: parseFloat(formValue.b4),
                        b5: parseFloat(formValue.b5),
                        b6: parseFloat(formValue.b6),
                        mean_value: this.avgFlowSpeed,
                        mean_value_standby: this.averageStandbySpeed
                    })
                    if (this.orderline.airspeed_downflows.length == 1) {
                        this.measurement_number = 2
                        this.measurement_possible = true
                    } else if (this.orderline.airspeed_downflows.length > 1){
                        this.measurement_possible = false
                    }
                    this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
                    this.navBack()
                },
                err => {
                    this.headerServiceProvider.loading = false
                    this.orderline.airspeed_downflows.push({
                        measurement_number: parseFloat(this.measurement_number),
                        a1: parseFloat(formValue.a1),
                        a2: parseFloat(formValue.a2),
                        a3: parseFloat(formValue.a3),
                        a4: parseFloat(formValue.a4),
                        a5: parseFloat(formValue.a5),
                        a6: parseFloat(formValue.a6),
                        b1: parseFloat(formValue.b1),
                        b2: parseFloat(formValue.b2),
                        b3: parseFloat(formValue.b3),
                        b4: parseFloat(formValue.b4),
                        b5: parseFloat(formValue.b5),
                        b6: parseFloat(formValue.b6),
                        mean_value: this.avgFlowSpeed,
                        mean_value_standby: this.averageStandbySpeed
                    })
                    if (this.orderline.airspeed_downflows.length == 1) {
                        this.measurement_number = 2
                        this.measurement_possible = true
                    } else if (this.orderline.airspeed_downflows.length > 1){
                        this.measurement_possible = false
                    }
                    this.navBack()
                    this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
                }
            )
        })
    }
    navBack() {
        this.navCtrl.pop();
    }
}