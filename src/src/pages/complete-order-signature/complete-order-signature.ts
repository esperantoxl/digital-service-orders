import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { Storage } from '@ionic/storage';
import { OrderLineList } from './../order-line-list/order-line-list';
import { Component, ViewChild } from '@angular/core';
import { NavParams, NavController, ViewController, App } from 'ionic-angular';
import {SignaturePad} from 'angular2-signaturepad/signature-pad';


@Component({
  selector: 'complete-order-signature-page',
  templateUrl: 'complete-order-signature-page.html',
})
export class CompleteOrderSignaturePage {
  @ViewChild(SignaturePad) public signaturePad : SignaturePad;

  public signaturePadOptions : Object = {
    'minWidth': 2,
    'canvasWidth': 340,
    'canvasHeight': 200
  };
  public signatureImage : string;
  public order: any
  public errorMessage: string

  constructor(
    public navCtrl: NavController, 
    public storage: Storage, 
    public ordersServiceProvider: OrdersServiceProvider,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public appCtrl: App,
    public headerServiceProvider: HeaderServiceProvider
  ) {
    this.order = navParams.get('order')
  }

  //Other Functions

  drawCancel() {
    this.navCtrl.pop()
  }

  drawComplete() {
    this.headerServiceProvider.loading = true
    this.signatureImage = this.signaturePad.toDataURL();
    this.storage.set('signature_image', this.signatureImage)
    this.storage.get('token').then(token => {
      this.ordersServiceProvider.closeOrder(this.order.id, token, this.signatureImage).subscribe(
        data => {
          this.order.status = 'Approved by Customer'
          this.appCtrl.getRootNav().popToRoot().then(() => {
            this.navCtrl.pop()
            this.headerServiceProvider.loading = false
          })
        },
        err => {
          // this.order.status = 'Approved by Customer'
          // this.storage.get('orders').then(orders => {
          //   let orderIndex
          //   for (let i = 0; i < orders.length; i++) {
          //     let order = orders[i];
          //     if (order.id === this.order.id) {
          //       orderIndex = i
          //     }
          //   }
          //   orders[orderIndex] = this.order
          //   this.storage.set('orders', orders)
          // })
          // this.appCtrl.getRootNav().popToRoot().then(() => {
          //   this.navCtrl.pop()
          //   this.headerServiceProvider.loading = false
          // })
          this.headerServiceProvider.loading = false
          this.errorMessage = "Je moet online zijn om de order te sluiten"
        }
      )
    })
  }

  drawClear() {
    this.signaturePad.clear();
  }

  canvasResize() {
    let canvas = document.querySelector('canvas');
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.set('canvasWidth', canvas.offsetWidth);
    this.signaturePad.set('canvasHeight', canvas.offsetHeight);
  }

  ngAfterViewInit() {
    this.signaturePad.clear();
    this.canvasResize();
  }

}