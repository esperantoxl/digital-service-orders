import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';


@Component({
  selector: 'form-components-page',
  templateUrl: 'form-components-page.html'
})

export class FormComponentsPage {
  orderline: any
  formStructure: any

  constructor(
    private navParams: NavParams,
    public storage: Storage,
    private navCtrl: NavController,
    public validationFormServiceProvider: ValidationFormServiceProvider,
    public headerServiceProvider: HeaderServiceProvider,
    private ordersServiceProvider: OrdersServiceProvider
  ) {
    this.orderline = this.navParams.get('orderline')
    this.formStructure = this.navParams.get('formStructure')
    if(!this.orderline.component_controls[0]) {
      this.orderline.component_controls[0] = {}
    }
  }

  submitFormHandler(formValue) {
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.validationFormServiceProvider.postComponentForm(token, this.orderline.id, formValue).subscribe(
        data => {
          for (var key in data) {
            if (data.hasOwnProperty(key)) { 
              var prop = data[key];
              this.orderline.component_controls[0][key] = prop
            }
          }
          this.headerServiceProvider.loading = false
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        },
        err => {
          for (var key in formValue) {
            if (formValue.hasOwnProperty(key)) { 
              var prop = formValue[key];
              this.orderline.component_controls[0][key] = prop
            }
          }
          this.headerServiceProvider.loading = false
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        }
      )
    })
  }

  navBack() {
    this.navCtrl.pop()
  }
}