import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { FormNavigationPage } from './../form-navigation-page/form-navigation-page';
import { EquipmentServiceProvider } from './../../providers/equipment-service/equipment-service';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Component } from '@angular/core';

@Component ({
  selector: 'page-equipment',
  templateUrl: 'equipment.html'
})

export class EquipmentPage {
  equipment: any
  pageTitle: string = 'Meetapparatuur'
  order: any
  orderline: any
  validation: boolean
  formNavDefinition: any
  checks: any = []

  constructor(
    public navCtrl: NavController, 
    public storage: Storage,
    public headerServiceProvider: HeaderServiceProvider,
    private navParams: NavParams,
    public equipmentServiceProvider: EquipmentServiceProvider,
    public validationFormServiceProvider: ValidationFormServiceProvider,
    private ordersServiceProvider: OrdersServiceProvider
  ) {
      this.orderline = navParams.get('selected_orderline')
      this.order = navParams.get('selected_order')

      this.storage.get('equipment').then(equipment => {
        this.equipment = equipment
        
        for (let i = 0; i < equipment.length; i++) {
          let element = equipment[i];
          let matches = 0

          for (let i = 0; i < this.orderline.equipment_used.length; i++) {
            let equipment_used = this.orderline.equipment_used[i]  
            if (equipment_used.from_equipment === element.copy_uid) {
              matches++
            }
          } 
          if (matches > 0) {
            element.checked = true
          }
          this.checks.push(element)
        }
      })
      
      if(this.orderline.service_type === 'Validation') {
        this.validation = true
      } else {
        this.validation = false
      }
  }

  selectEquipment(equipmentIds) {
    let actualEquipmentIds = new Array;
    for (let key in equipmentIds) {
      let value: boolean = equipmentIds[key]
      if (value) {
        actualEquipmentIds.push(parseInt(key))
      }
    }

    this.headerServiceProvider.loading = true

    this.storage.get('token').then(token => {
      this.equipmentServiceProvider.selectUserEquipment(actualEquipmentIds, this.orderline.id, token).subscribe(
        data => {
          this.headerServiceProvider.loading = false
          this.navCtrl.pop()
          this.orderline.equipment_used = data
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
        },
        err => {
          this.orderline.equipment_used = []
          this.storage.get('equipment').then(equipment => {
            for (let i = 0; i < actualEquipmentIds.length; i++) {
              let actualEquipmentId = actualEquipmentIds[i]

              for (let eI = 0; eI < equipment.length; eI++){
                if (actualEquipmentId === equipment[eI].id) {
                  const equipmentToPush = {
                    from_equipment: equipment[eI].copy_uid
                  }
                  this.orderline.equipment_used.push(equipmentToPush)
                }
              }
            }
          })
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.headerServiceProvider.loading = false
          this.navCtrl.pop()
        })
      }
    );
  }

  equipmentIsSelected(copy_uid) {
  }

  navBack() {
      this.navCtrl.pop();
  }
}
