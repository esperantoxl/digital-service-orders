import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';


@Component({
  selector: 'form-leakage-hepa-page',
  templateUrl: 'form-leakage-hepa-page.html'
})

export class FormLeakageHepaPage {
  orderline: any
  formStructure: any
  heighestDownflowValue: number
  downflowValues: any = []

  constructor(
    private navParams: NavParams,
    public storage: Storage,
    private navCtrl: NavController,
    public headerServiceProvider: HeaderServiceProvider,
    public validationFormServiceProvider: ValidationFormServiceProvider,
    private ordersServiceProvider: OrdersServiceProvider
  ) {
    this.orderline = this.navParams.get('orderline')
    this.formStructure = this.navParams.get('formStructure')
    let downFlowKeys = Object.keys(this.orderline.leakage_hepas[0]).filter(key => {
      return key.substr(0, 5) === 'df_p_'
    })
    for (let i = 0; i < downFlowKeys.length; i++) {
      let element = downFlowKeys[i];
      this.downflowValues.push(this.orderline.leakage_hepas[0][element])
    }
    this.getHighestDownflow(false, false)
  }
  submitFormHandler(formData) {
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.validationFormServiceProvider.postLeakageHepa(token, this.orderline.id, formData, this.orderline.protocol.hepa_leakage_method).subscribe(
        data => {
          for (var key in data) {
            if (data.hasOwnProperty(key)) { 
              var prop = data[key];
              this.orderline.leakage_hepas[0][key] = prop
            }
          }
          this.headerServiceProvider.loading = false
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        },
        err => {
          this.headerServiceProvider.loading = false
          for (var key in formData) {
            if (formData.hasOwnProperty(key)) { 
              var prop = formData[key];
              this.orderline.leakage_hepas[0][key] = prop
            }
          }
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        }
      )
    })
  }

  getHighestDownflow(val, i) {
    if (val) {
      this.downflowValues[i] = val
    }
    this.heighestDownflowValue = this.downflowValues[0];

    for (let i = 1; i < this.downflowValues.length; i++) {
      if (this.downflowValues[i] > this.heighestDownflowValue) {
        this.heighestDownflowValue = this.downflowValues[i];
      }
    }
  }

  navBack() {
    this.navCtrl.pop()
  }
}