import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'order-comments-page',
  templateUrl: 'order-comments-page.html'
})

export class OrderCommentsPage {
  order: any

  constructor(
    private navParams: NavParams,
    public storage: Storage,
    private navCtrl: NavController,
    public headerServiceProvider: HeaderServiceProvider,
    public ordersServiceProvider: OrdersServiceProvider
  ) {
    this.order = this.navParams.get('order')
    if (!this.order.communication_logs) {
      this.order.communication_logs = [{
        comment: ''
      }]
    }
  }

  submitFormHandler(formValue) {
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.ordersServiceProvider.postOrderComment(this.order.id, token, formValue).subscribe(
        data => {
          this.headerServiceProvider.loading = false
          this.navBack()
        },
        err => {
          this.headerServiceProvider.loading = false
          this.navBack()
          this.order.communication_logs[0].comment = formValue.comment
          this.storage.get('orders').then(orders => {
            let orderIndex
            for (let i = 0; i < orders.length; i++) {
              let order = orders[i];
              if (order.id === this.order.id) {
                orderIndex = i
              }
            }
            orders[orderIndex] = this.order
            this.storage.set('orders', orders)
          })
        }
      )
    })
  }

  navBack() {
    this.navCtrl.pop()
  }
}