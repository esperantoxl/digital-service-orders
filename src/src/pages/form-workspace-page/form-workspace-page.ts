import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';


@Component({
  selector: 'form-workspace-page',
  templateUrl: 'form-workspace-page.html'
})

export class FormWorkspacePage {
  orderline: any
  formStructure: any
  sampleCount: number = 20
  left_03: number = 0
  left_05: number = 0
  right_03: number = 0
  right_05: number = 0
  left_03_calculated: number
  left_05_calculated: number
  right_03_calculated: number
  right_05_calculated: number

  constructor(
    private navParams: NavParams,
    public storage: Storage,
    private navCtrl: NavController,
    public headerServiceProvider: HeaderServiceProvider,
    public validationFormServiceProvider: ValidationFormServiceProvider,
    private ordersServiceProvider: OrdersServiceProvider
  ) {
    this.orderline = this.navParams.get('orderline')
    this.formStructure = this.navParams.get('formStructure')
    this.calcAll()
  }

  ionViewDidEnter () {
    this.left_03 = this.orderline.classification_workspaces[0].left_03
    this.left_05 = this.orderline.classification_workspaces[0].left_05
    this.right_03 = this.orderline.classification_workspaces[0].right_03
    this.right_05 = this.orderline.classification_workspaces[0].right_05
    this.left_03_calculated = this.orderline.classification_workspaces[0].left_03_calculated
    this.left_05_calculated = this.orderline.classification_workspaces[0].left_05_calculated
    this.right_03_calculated = this.orderline.classification_workspaces[0].right_03_calculated
    this.right_05_calculated = this.orderline.classification_workspaces[0].right_05_calculated
  }
  
  changeSampleCount(multiplier) {
    this.sampleCount = multiplier
    this.calcAll()
  }
  
  calcAll() {
    this.calcSample('left_03')
    this.calcSample('left_05')
    this.calcSample('right_03')
    this.calcSample('right_05')
  }

  calcSample(sample) {
    let sampleCalcName = sample + '_calculated'
    let val = this[sample]
    
    this[sampleCalcName] = val * this.sampleCount
  }

  submitFormHandler(formData) {
    let calculated = {
      left_03_calculated: this.left_03_calculated,
      left_05_calculated: this.left_05_calculated,
      right_03_calculated: this.right_03_calculated,
      right_05_calculated: this.right_05_calculated
    }
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.validationFormServiceProvider.postWorkspace(token, this.orderline.id, formData, calculated).subscribe(
        data => {
          this.headerServiceProvider.loading = false
          this.orderline.classification_workspaces[0].sample_volume_counter_list = formData.sample_volume_counter_list
          this.orderline.classification_workspaces[0].left_03 = this.left_03
          this.orderline.classification_workspaces[0].left_05 = this.left_05
          this.orderline.classification_workspaces[0].right_03 = this.right_03
          this.orderline.classification_workspaces[0].right_05 = this.right_05
          this.orderline.classification_workspaces[0].left_03_calculated = this.left_03_calculated
          this.orderline.classification_workspaces[0].left_05_calculated = this.left_05_calculated
          this.orderline.classification_workspaces[0].right_03_calculated = this.right_03_calculated
          this.orderline.classification_workspaces[0].right_05_calculated = this.right_05_calculated
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        },
        err => {
          this.headerServiceProvider.loading = false
          this.orderline.classification_workspaces[0].sample_volume_counter_list = formData.sample_volume_counter_list
          this.orderline.classification_workspaces[0].left_03 = this.left_03
          this.orderline.classification_workspaces[0].left_05 = this.left_05
          this.orderline.classification_workspaces[0].right_03 = this.right_03
          this.orderline.classification_workspaces[0].right_05 = this.right_05
          this.orderline.classification_workspaces[0].left_03_calculated = this.left_03_calculated
          this.orderline.classification_workspaces[0].left_05_calculated = this.left_05_calculated
          this.orderline.classification_workspaces[0].right_03_calculated = this.right_03_calculated
          this.orderline.classification_workspaces[0].right_05_calculated = this.right_05_calculated
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        }
      )
    })
  }

  navBack() {
    this.navCtrl.pop()
  }
}