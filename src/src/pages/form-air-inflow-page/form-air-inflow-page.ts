import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
    selector: 'form-air-inflow-page',
    templateUrl: 'form-air-inflow-page.html'
})

export class FormAirInflowPage {
    orderline: any
    navDefinition: any
    formStructure: any
    measurement_number: any = 1
    measurement_possible: any = true
    criteria: any = {
        operator: '>=',
        value: '0.4'
    }
    value: any = {
    }
    avgFlowSpeed: number = 0

    constructor(
        private navParams: NavParams,
        public storage: Storage,
        private navCtrl: NavController,
        public validationFormServiceProvider: ValidationFormServiceProvider,
        public headerServiceProvider: HeaderServiceProvider,
        public ordersServiceProvider: OrdersServiceProvider
    ) {
        this.orderline = this.navParams.get('orderline')
        console.log(this.orderline)
        this.formStructure = this.navParams.get('formStructure')
        if (this.orderline.airspeed_inflows.length == 1) {
            this.measurement_number = 2
            this.measurement_possible = true
        } else if (this.orderline.airspeed_inflows.length > 1){
            this.measurement_possible = false
        }
    }

    calcAverage(value) {
        let sumary: number = 0
        let numOfKeys = 0
        for (var key in value) {
            if (value.hasOwnProperty(key)) {
                sumary += parseFloat(value[key])
                numOfKeys++;
            }
        }
        let avg = sumary/numOfKeys || 0
        this.avgFlowSpeed = Math.round(avg * 100) / 100
    }

    submitFormHandler(formValue){
        this.headerServiceProvider.loading = true
        formValue['mean_value'] = this.avgFlowSpeed
        this.storage.get('token').then(token => {
            this.validationFormServiceProvider.postAirInflowForm(token, this.orderline.id, formValue, this.measurement_number).subscribe(
                data => {
                    this.headerServiceProvider.loading = false
                    this.orderline.airspeed_inflows.push({
                        measurement_number: parseFloat(data.measurement_number),
                        a1: parseFloat(data.a1),
                        a2: parseFloat(data.a2),
                        a3: parseFloat(data.a3),
                        b1: parseFloat(data.b1),
                        b2: parseFloat(data.b2),
                        b3: parseFloat(data.b3),
                        c1: parseFloat(data.c1),
                        c2: parseFloat(data.c2),
                        c3: parseFloat(data.c3),
                        smoke_test: data.smoke_test,
                        mean_value: this.avgFlowSpeed
                    })
                    if (this.orderline.airspeed_inflows.length == 1) {
                        this.measurement_number = 2
                        this.measurement_possible = true
                    } else if (this.orderline.airspeed_inflows.length > 1){
                        this.measurement_possible = false
                    }
                    this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
                    this.navBack()
                },
                err => {
                    this.headerServiceProvider.loading = false
                    this.headerServiceProvider.loading = false
                    this.orderline.airspeed_inflows.push({
                        measurement_number: parseFloat(this.measurement_number),
                        a1: parseFloat(formValue.a1),
                        a2: parseFloat(formValue.a2),
                        a3: parseFloat(formValue.a3),
                        b1: parseFloat(formValue.b1),
                        b2: parseFloat(formValue.b2),
                        b3: parseFloat(formValue.b3),
                        c1: parseFloat(formValue.c1),
                        c2: parseFloat(formValue.c2),
                        c3: parseFloat(formValue.c3),
                        smoke_test: formValue.smoke_test,
                        mean_value: this.avgFlowSpeed
                    })
                    if (this.orderline.airspeed_inflows.length == 1) {
                        this.measurement_number = 2
                        this.measurement_possible = true
                    } else if (this.orderline.airspeed_inflows.length > 1){
                        this.measurement_possible = false
                    }
                    this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
                    this.navBack()
                }
            )
        })
    }
    
    navBack() {
        this.navCtrl.pop();
    }
}