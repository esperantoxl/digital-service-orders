import { EquipmentServiceProvider } from './../../providers/equipment-service/equipment-service';
import { OrderLineList } from './../order-line-list/order-line-list';
import { Storage } from '@ionic/storage';
import { LoginPage } from './../login/login';
import { NavController } from 'ionic-angular';
import { UserServiceProvider } from './../../providers/user-service/user-service';
import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Component } from '@angular/core';

@Component ({
  selector: 'page-orders',
  templateUrl: 'orders.html'
})

export class OrdersPage {
  orders: any = []
  completed_orders: any = []
  pageTitle: string = 'Orders'
  orderloading: boolean = false
  firstLoad: boolean = true
  offlineChanges: boolean = false
  hasOrders: boolean = false

  constructor(
    public navCtrl: NavController, 
    public ordersServiceProvider: OrdersServiceProvider, 
    public userServiceProvider: UserServiceProvider,
    public storage: Storage,
    public headerServiceProvider: HeaderServiceProvider,
    public equipmentServiceProvider: EquipmentServiceProvider
  ) {
    this.storage.remove('selected_orderline')

    this.getOrders().then(() => {
      if (this.hasOrders) {
        this.checkForOrderlineEdits().then(() => {
          if (!this.headerServiceProvider.offline) {
            this.pullOrders()
            this.getEquipment()
          }
        })
      } else {
        this.pullOrders()
        this.getEquipment()
      }
    })
  }

  ionViewDidEnter () {
    console.log
    if (this.hasOrders) {
      this.checkForOrderlineEdits()
    }
  }

  pullOrders () {
    this.requestOrders()
  }

  pushOrders () {
    this.storage.get('orders').then(orders => {
      let match
      for (let i = 0; i < orders.length; i++) {
        let order = orders[i];
        
        let edited = order.orderlines.filter((orderline) =>{
          return orderline.changedOffline
        })
        this.storage.get('token').then(token => {
          this.ordersServiceProvider.pushOrderlines(token, edited).subscribe(
            data => {
              console.log(data)
              this.pullOrders()
            },
            err => {
              console.error(err)
            }
          )
        })
      }
    })
  }

  getOrders() {
    return new Promise((resolve, reject) => {
      this.storage.get('orders').then(orders => {
        this.orders = orders
        this.hasOrders = true
        resolve()
      })
      this.storage.get('completed_orders').then(completed => {
        this.completed_orders = completed
      })
    })
  }

  checkForOrderlineEdits () {
    return new Promise((resolve, reject) => {
      this.storage.get('orders').then(orders => {
        if (orders) {
          for (let i = 0; i < orders.length; i++) {
            let order = orders[i];
            
            let edited = order.orderlines.filter((orderline) =>{
              return orderline.changedOffline
            })
            if (edited.length > 0) {
              this.offlineChanges = true
            } else this.offlineChanges = false
          }
        }
        resolve()
      })
    })
  }

  requestOrders() {
    this.orders = []
    this.completed_orders = []
    this.orderloading = true
    this.storage.get('token').then(token =>{
      this.ordersServiceProvider.getUserOrders(token).subscribe(
        data => {
          this.orders = data.filter(order => {
            return order.status === 'Open' || order.status === 'Assigned' || order.status === 'In progress'
          })
          this.completed_orders = data.filter(order => {
            return order.status != 'Open' && order.status != 'Assigned' && order.status != 'In progress'
          })
          this.storage.set('orders', this.orders).then(() => {
            this.checkForOrderlineEdits()
          })
          this.storage.set('completed_orders', this.completed_orders)
          this.orderloading = false
          if (this.orders.length > 0) this.hasOrders = true
        },
        err => {
          this.orderloading = false
        }
      )
    })
  }

  getEquipment(){
    this.storage.get('token').then(token => {
      this.equipmentServiceProvider.getUserEquipment(token).subscribe(
        data => {
          this.storage.set('equipment', data)
        },
        err => {
        }
      )
    })
  }
  
  openOrder(orderID) {
    this.headerServiceProvider.loading = true;
    const order = this.orders.find(order => {return order.id === orderID})
    this.storage.set('order', order)
    this.navCtrl.push(OrderLineList, {
      order: order
    })
    this.headerServiceProvider.loading = false;
  }
}
