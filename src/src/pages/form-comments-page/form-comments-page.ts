import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'form-comments-page',
  templateUrl: 'form-comments-page.html'
})

export class FormCommentsPage {
  orderline: any
  formStructure: any

  constructor(
    private navParams: NavParams,
    public storage: Storage,
    private navCtrl: NavController,
    public validationFormServiceProvider: ValidationFormServiceProvider,
    public headerServiceProvider: HeaderServiceProvider,
    public ordersServiceProvider: OrdersServiceProvider
  ) {
    this.orderline = this.navParams.get('orderline')
    this.formStructure = this.navParams.get('formStructure')
  }

  submitFormHandler(formValue) {
    console.log(formValue)
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.validationFormServiceProvider.postCommentForm(token, this.orderline.id, formValue).subscribe(
        data => {
          for (var key in data) {
            if (data.hasOwnProperty(key)) { 
              var prop = data[key];
              this.orderline.communication_logs[0][key] = prop
            }
          }
          this.headerServiceProvider.loading = false
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        },
        err => {
          this.headerServiceProvider.loading = false
          this.orderline.communication_logs[0].comment = formValue.comment
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        }
      )
    })
  }

  navBack() {
    this.navCtrl.pop()
  }
}