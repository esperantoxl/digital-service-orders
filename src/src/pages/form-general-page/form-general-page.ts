import { OrdersServiceProvider } from './../../providers/orders-service/orders-service';
import { FormComponentsPage } from './../form-components-page/form-components-page';
import { FormAirDownflowPage } from './../form-air-downflow-page/form-air-downflow-page';
import { EquipmentPage } from './../equipment/equipment';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { FormAirInflowPage } from './../form-air-inflow-page/form-air-inflow-page';
import { Storage } from '@ionic/storage';
import { ValidationFormServiceProvider } from './../../providers/validation-form-service/validation-form-service';
import { NavParams, NavController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'form-general-page',
  templateUrl: 'form-general-page.html'
})

export class FormGeneralPage {
  orderline: any
  navDefinition: any
  formStructure: any
  filters: any

  constructor(
    private navParams: NavParams,
    public validationFormServiceProvider: ValidationFormServiceProvider,
    public storage: Storage,
    private navCtrl: NavController,
    public headerServiceProvider: HeaderServiceProvider,
    public ordersServiceProvider: OrdersServiceProvider
  ) {
    this.orderline = this.navParams.get('orderline')
    this.navDefinition = this.orderline.form_general_structure
    this.formStructure = this.navParams.get('formStructure')
    this.filters = this.orderline.service_article.components.filter(component => {
      return component.group === 'Filter'
    })
  }
  
  navBack() {
    this.navCtrl.pop()
  }

  submitFormHandler(formData) {
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.validationFormServiceProvider.postGeneralForm(token, this.orderline.id, formData).subscribe(
        data => {
          for (var key in data) {
            if (data.hasOwnProperty(key)) { 
              var prop = data[key];
              this.orderline.general_measurements[0][key] = prop
            }
          }
          this.orderline.testvalue = 'general'
          this.headerServiceProvider.loading = false
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        },
        err => {
          this.headerServiceProvider.loading = false
          this.orderline.general_measurements[0].drain_connection = formData.drain_connection,
          this.orderline.general_measurements[0].drain_depth = formData.drain_depth,
          this.orderline.general_measurements[0].drain_length = formData.drain_length,
          this.orderline.general_measurements[0].drain_height = formData.drain_height,
          this.orderline.general_measurements[0].drain_pressure = formData.drain_pressure,
          this.orderline.general_measurements[0].drain_width = formData.drain_width,
          this.orderline.general_measurements[0].fan_operation_mode = formData.fan_operation_mode,
          this.orderline.general_measurements[0].fan_standby_mode = formData.fan_standby_mode,
          this.orderline.general_measurements[0].nen_executed = formData.nen_executed,
          this.orderline.general_measurements[0].prefilter_changed = formData.prefilter_changed,
          this.orderline.general_measurements[0].sa_operating_hours = formData.sa_operating_hours
          
          this.ordersServiceProvider.saveOrderlineToStorage(this.orderline)
          this.navBack()
        }
      )
    })
  }
}