import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class OrdersServiceProvider {
  private GET_USER_ORDER_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/orders/get-user-orders'
  private GET_ORDERLINES_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/orders/get-orderlines'
  private UPDATE_STATUS_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/orders/update-status'
  private POST_COMMENT_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/comments/post-comment'
  private COMPLETE_ORDER_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/orders/complete-order'
  private UPDATE_ORDERLINE_STATUS_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/orderline/update-status'
  private CLOSE_ORDERLINE_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/orderline/close-orderline'
  private PUSH_ORDERLINES_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/orders/push-orderlines'
  contentHeader = new Headers({"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"});

  constructor(public http: Http, private storage: Storage) {
  }

  getUserOrders(token) {
    return this.http.get(this.GET_USER_ORDER_URL + '?session_token=' + token, { headers: this.contentHeader }).map(res => res.json())
  }

  getOrderLines(orderID, token) {
    return this.http.get(this.GET_ORDERLINES_URL + '?session_token=' + token + '&order_id=' + orderID, { headers: this.contentHeader }).map(res => res.json())
  }

  setOrderStatus(orderID, token, status) {
    return this.http.post(this.UPDATE_STATUS_URL, {order_id: orderID, session_token: token, status: status}, {headers: this.contentHeader}).map(res => res.json())
  }

  postOrderComment(orderID, token, formData) {
    return this.http.post(this.POST_COMMENT_URL, {order_id: orderID, session_token: token, comment: formData.comment}, {headers: this.contentHeader}).map(res => res.json())
  }

  setOrderlineStatus(orderlineID, token, status) {
    return this.http.post(this.UPDATE_ORDERLINE_STATUS_URL, {orderline_id: orderlineID, session_token: token, status: status}, {headers: this.contentHeader}).map(res => res.json())
  }

  closeOrderline(orderlineID, token) {
    return this.http.post(this.CLOSE_ORDERLINE_URL, {orderline_id: orderlineID, session_token: token}, {headers: this.contentHeader})
  }
  
  closeOrder(orderID, token, signature) {
    return this.http.post(this.COMPLETE_ORDER_URL, {order_id: orderID, session_token: token, signature}, {headers: this.contentHeader})
  }

  pushOrderlines (token, orderlines) {
    return this.http.post(this.PUSH_ORDERLINES_URL, {orderlines, session_token: token}, {headers: this.contentHeader}).map(res => res.json())
  }

  saveOrderlineToStorage (orderlineToSave) {
    this.storage.get('orders').then(orders => {
      let orderIndex
      for (let i = 0; i < orders.length; i++) {
        let order = orders[i];
        if (order.id === orderlineToSave.order_id) {
          orderIndex = i
        }
      }
      let orderlineIndex
      for (let i = 0; i < orders[orderIndex].orderlines.length; i++) {
        let orderline = orders[orderIndex].orderlines[i];
        if (orderline.id === orderlineToSave.id) {
          orderlineIndex = i
        }
      }
      orders[orderIndex].orderlines[orderlineIndex] = orderlineToSave
      this.storage.set('orders', orders)
    })
  }
}
