import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class EquipmentServiceProvider {
  private GET_EQUIPMENT_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/equipment/get-user-equipment'
  private SELECT_EQUIPMENT_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/equipment/select-orderline-equipment'
  contentHeader = new Headers({"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"});

  constructor(public http: Http) {
  }

  getUserEquipment (token) {
      return this.http.get(this.GET_EQUIPMENT_URL + '?session_token=' + token,  { headers: this.contentHeader }).map(res => res.json())
  }

  selectUserEquipment(equipmentIds, orderlineId, token) {
    return this.http.post(this.SELECT_EQUIPMENT_URL, {session_token: token, equipment_ids: equipmentIds, orderline_id: orderlineId}, {headers: this.contentHeader}).map(res => res.json())
  }
}
