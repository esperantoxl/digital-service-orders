import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ValidationFormServiceProvider {
  GET_FORM_STRUCTURE_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/orderline/get-form-structure'
  GET_INFLOW_STRUCTURE_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/airspeed-validation/get-inflow-form'
  POST_INFLOW_FORM_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/airspeed-validation/inflow-form'
  POST_DOWNFLOW_FORM_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/airspeed-validation/downflow-form'
  VALIDATE_COMPONENTS_FORM_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/components/validate-components'
  VALIDATE_GENERAL_FORM_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/general-validation/post-general'
  COMMENTS_FORM_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/comments/post-comment'
  POST_LEAKAGE_HEPA_FORM_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/leakage/hepa'
  POST_LEAKAGE_PLENUM_FORM_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/leakage/plenum'
  POST_WORKSPACE_FORM_URL: string = 'https://atdso-dev.bettywebblocks.com/api/dso/workspace/classification'
  contentHeader = new Headers({"Content-Type": "application/json"});

  constructor(public http: Http) {
  }

  getFormStructure(token, orderlineID) {
    return this.http.get(this.GET_FORM_STRUCTURE_URL + '?session_token=' + token + '&orderline_id=' + orderlineID, {headers: this.contentHeader}).map(res => res.json())
  }

  getAirInflowStructure(token, orderlineID) {
    return this.http.get(this.GET_INFLOW_STRUCTURE_URL + '?session_token=' + token + '&orderline_id=' + orderlineID, {headers: this.contentHeader}).map(res => res.json())
  }

  postAirDownflowForm(token, orderlineID, formData, measurement_number) {
    return this.http.post(this.POST_DOWNFLOW_FORM_URL, {
      session_token: token, 
      orderline_id: orderlineID,
      measurement_number: measurement_number,
      a1: parseFloat(formData.a1),
      a2: parseFloat(formData.a2),
      a3: parseFloat(formData.a3),
      a4: parseFloat(formData.a4),
      a5: parseFloat(formData.a5),
      a6: parseFloat(formData.a6),
      b1: parseFloat(formData.b1),
      b2: parseFloat(formData.b2),
      b3: parseFloat(formData.b3),
      b4: parseFloat(formData.b4),
      b5: parseFloat(formData.b5),
      b6: parseFloat(formData.b6),
      average_standby_speed: formData.average_standby_speed,
      mean_value: formData.mean_value
    }, {headers: this.contentHeader}).map(res => res.json())
  }

  postLeakagePlenum(token, orderlineID, formData) {
    return this.http.post(this.POST_LEAKAGE_PLENUM_FORM_URL, {
      session_token: token,
      orderline_id: orderlineID,
      leakage_location: formData.leakage_location,
      concentration_seam_max: formData.concentration_seam_max,
      concentration_background: formData.concentration_background
    }, {headers: this.contentHeader}).map(res => res.json())
  }

  postWorkspace(token, orderlineID, formData, calculated) {
    return this.http.post(this.POST_WORKSPACE_FORM_URL, {
      session_token: token,
      orderline_id: orderlineID,
      left_03: formData.left_03,
      left_05: formData.left_05,
      right_03: formData.right_03,
      right_05: formData.right_05,
      left_03_calculated: calculated.left_03_calculated,
      left_05_calculated: calculated.left_05_calculated,
      right_03_calculated: calculated.right_03_calculated,
      right_05_calculated: calculated.right_05_calculated,
      sample_volume_counter_list: formData.sample_volume_counter_list
    }, {headers: this.contentHeader}).map(res => res.json())
  }

  postLeakageHepa(token, orderlineID, formData, method) {
    return this.http.post(this.POST_LEAKAGE_HEPA_FORM_URL, {
      session_token: token,
      measurement_method: method,
      orderline_id: orderlineID,
      df_p_b12: parseFloat(formData.df_p_b12),
      df_p_b5: parseFloat(formData.df_p_b5),
      df_p_b7: parseFloat(formData.df_p_b7),
      df_p_b2: parseFloat(formData.df_p_b2),
      df_p_b3: parseFloat(formData.df_p_b3),
      df_p_b10: parseFloat(formData.df_p_b10),
      df_p_b8: parseFloat(formData.df_p_b8),
      df_p_a1: parseFloat(formData.df_p_a1),
      df_p_a9: parseFloat(formData.df_p_a9),
      df_p_b6: parseFloat(formData.df_p_b6),
      df_p_a4: parseFloat(formData.df_p_a4),
      df_p_a8: parseFloat(formData.df_p_a8),
      df_p_a6: parseFloat(formData.df_p_a6),
      df_p_a7: parseFloat(formData.df_p_a7),
      ef_p_a1: parseFloat(formData.ef_p_a1),
      df_p_a11: parseFloat(formData.df_p_a11),
      df_p_b11: parseFloat(formData.df_p_b11),
      df_p_a2: parseFloat(formData.df_p_a2),
      df_p_b1: parseFloat(formData.df_p_b1),
      df_p_a3: parseFloat(formData.df_p_a3),
      df_p_a12: parseFloat(formData.df_p_a12),
      df_p_b4: parseFloat(formData.df_p_b4),
      df_p_a5: parseFloat(formData.df_p_a5),
      df_p_a10: parseFloat(formData.df_p_a10),
      df_p_b9: parseFloat(formData.df_p_b9),
      ff_aerosol: parseFloat(formData.ff_aerosol),
      ef_aerosol: parseFloat(formData.ef_aerosol),
      df_pressure: parseFloat(formData.df_pressure),
      ff_pressure: parseFloat(formData.ff_pressure),
      df_aerosol: parseFloat(formData.df_aerosol),
    }, {headers: this.contentHeader}).map(res => res.json())
  }

  postAirInflowForm(token, orderlineID, formData, measurement_number) {
    return this.http.post(this.POST_INFLOW_FORM_URL, {
      session_token: token, 
      orderline_id: orderlineID,
      measurement_number: measurement_number,
      a1: parseFloat(formData.a1),
      a2: parseFloat(formData.a2),
      a3: parseFloat(formData.a3),
      b1: parseFloat(formData.b1),
      b2: parseFloat(formData.b2),
      b3: parseFloat(formData.b3),
      c1: parseFloat(formData.c1),
      c2: parseFloat(formData.c2),
      c3: parseFloat(formData.c3),
      smoke_test: formData.smoke_test,
      mean_value: formData.mean_value
    }, {headers: this.contentHeader}).map(res => res.json())
  }

  postComponentForm(token, orderlineID, formData) {
    return this.http.post(this.VALIDATE_COMPONENTS_FORM_URL, {
      session_token: token, 
      orderline_id: orderlineID,
      alarm_indicator: formData.alarm_indicator,
      electrical_system: formData.electrical_system,
      gas_hose: formData.gas_hose,
      night_door: formData.night_door,
      return_valve: formData.return_valve,
      uv_light: formData.uv_light,
      uv_light_hours: formData.uv_light_hours,
      window_alarm: formData.window_alarm
    }, {headers: this.contentHeader}).map(res => res.json())
  }

  postGeneralForm(token, orderlineID, formData) {
    return this.http.post(this.VALIDATE_GENERAL_FORM_URL, {
      session_token: token, 
      orderline_id: orderlineID,
      drain_connection: formData.drain_connection,
      drain_depth: formData.drain_depth,
      drain_length: formData.drain_length,
      drain_height: formData.drain_height,
      drain_pressure: formData.drain_pressure,
      drain_width: formData.drain_width,
      fan_operation_mode: formData.fan_operation_mode,
      fan_standby_mode: formData.fan_standby_mode,
      nen_executed: formData.nen_executed,
      prefilter_changed: formData.prefilter_changed,
      sa_operating_hours: formData.sa_operating_hours
    }, {headers: this.contentHeader}).map(res => res.json())
  }

  postCommentForm(token, orderlineID, formData) {
    return this.http.post(this.COMMENTS_FORM_URL, {
      session_token: token, 
      orderline_id: orderlineID,
      comment: formData.comment
    }, {headers: this.contentHeader}).map(res => res.json())
  }

}