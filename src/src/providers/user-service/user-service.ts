import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class UserServiceProvider {
  error: any;
  user: any; 

  contentHeader = new Headers({"Content-Type": "application/json"});
  private LOGIN_URL = "https://atdso-dev.bettywebblocks.com/api/dso/signin/login";
  private LOGOUT_URL = "https://atdso-dev.bettywebblocks.com/api/dso/signin/logout";
  private LOGIN_CHECK_URL = "https://atdso-dev.bettywebblocks.com/api/dso/signin/check-if-logged-in";

  constructor(public http: Http, public storage: Storage) {
  }

  login(credentials) {
    return this.http.post(this.LOGIN_URL, JSON.stringify(credentials), { headers: this.contentHeader }).map(res => res.json())
  }

  isLoggedIn(token) {
    return this.http.post(this.LOGIN_CHECK_URL, {session_token: token}, { headers: this.contentHeader }).map(res => res.json())
  }

  logout(token) {
    return this.http.post(this.LOGOUT_URL, {session_token: token}, {headers: this.contentHeader}).map(res => res.json())
  }
}
