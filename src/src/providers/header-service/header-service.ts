import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';

@Injectable()
export class HeaderServiceProvider {
  public loading: boolean = false
  public offline: boolean

  constructor(private http: Http, private network: Network) {
    this.http.get('https://atdso-dev.bettywebblocks.com').toPromise().then(response => {
    }).catch(err => {
      if(err.status === 0) {
        this.offline= true
      } else this.offline = false
    })

    this.network.onDisconnect().subscribe(() => {
      this.offline= true
    });

    this.network.onConnect().subscribe(() => {
      this.offline = false
    });
  }
}
