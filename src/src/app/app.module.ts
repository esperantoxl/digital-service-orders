import { Network } from '@ionic-native/network';
import { OrderCommentsPage } from './../pages/order-comment-page/order-comments-page';
import { FormCommentsPage } from './../pages/form-comments-page/form-comments-page';
import { CompleteOrderSignaturePage } from './../pages/complete-order-signature/complete-order-signature';
import { FormWorkspacePage } from './../pages/form-workspace-page/form-workspace-page';
import { FormLeakagePlenumPage } from './../pages/form-leakage-plenum/form-leakage-plenum-page';
import { FormLeakageHepaPage } from './../pages/form-leakage-hepa/form-leakage-hepa-page';
import { FormGeneralPage } from './../pages/form-general-page/form-general-page';
import { FormComponentsPage } from './../pages/form-components-page/form-components-page';
import { FormAirDownflowPage } from './../pages/form-air-downflow-page/form-air-downflow-page';
import { FormAirInflowPage } from './../pages/form-air-inflow-page/form-air-inflow-page';
import { FormNavigationPage } from './../pages/form-navigation-page/form-navigation-page';
import { EquipmentPage } from './../pages/equipment/equipment';
import { OrderLineList } from './../pages/order-line-list/order-line-list';
import { HeaderPage } from './../shared/header/header';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from "@angular/common";
import { HttpModule, JsonpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { SignaturePadModule } from 'angular2-signaturepad';

import { OrdersPage } from './../pages/orders/orders';
import { LoginPage } from "../pages/login/login";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserServiceProvider } from '../providers/user-service/user-service';
import { OrdersServiceProvider } from '../providers/orders-service/orders-service';
import { HeaderServiceProvider } from './../providers/header-service/header-service';
import { ConvertCharCode } from "../pipes/convertCharCode.pipe";
import { EquipmentServiceProvider } from "../providers/equipment-service/equipment-service";
import { ValidationFormServiceProvider } from '../providers/validation-form-service/validation-form-service';

@NgModule({
  declarations: [
    MyApp,
    OrdersPage,
    LoginPage,
    HeaderPage,
    EquipmentPage,
    OrderLineList,
    FormNavigationPage,
    FormAirInflowPage,
    FormAirDownflowPage,
    CompleteOrderSignaturePage,
    FormComponentsPage,
    FormGeneralPage,
    FormLeakageHepaPage,
    FormLeakagePlenumPage,
    FormWorkspacePage,
    FormCommentsPage,
    OrderCommentsPage,
    ConvertCharCode
  ],
  imports: [
    BrowserModule,
    CommonModule,
    SignaturePadModule,
    HttpModule,
    JsonpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    OrdersPage,
    LoginPage,
    HeaderPage,
    EquipmentPage,
    OrderLineList,
    FormNavigationPage,
    FormAirInflowPage,
    FormAirDownflowPage,
    FormComponentsPage,
    FormLeakageHepaPage,
    FormLeakagePlenumPage,
    CompleteOrderSignaturePage,
    FormWorkspacePage,
    FormGeneralPage,
    FormCommentsPage,
    OrderCommentsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserServiceProvider,
    OrdersServiceProvider,
    HeaderServiceProvider,
    EquipmentServiceProvider,
    ValidationFormServiceProvider
  ]
})
export class AppModule {}