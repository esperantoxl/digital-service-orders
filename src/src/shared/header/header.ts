import { OrdersPage } from './../../pages/orders/orders';
import { LoginPage } from './../../pages/login/login';
import { UserServiceProvider } from './../../providers/user-service/user-service';
import { HeaderServiceProvider } from './../../providers/header-service/header-service';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';
import { Component, Input } from '@angular/core';

@Component ({
  selector: 'layout-header',
  templateUrl: 'header.html'
})

export class HeaderPage {
  @Input() pageTitle: string = null
  @Input() extraMenuItem: any = null
  pageIsLoading: any = false

  constructor(
    public navCtrl: NavController, 
    public userServiceProvider: UserServiceProvider,
    public storage: Storage,
    public headerServiceProvider: HeaderServiceProvider
  ) {
    if (this.extraMenuItem) {
      for (let i = 0; 1 < this.extraMenuItem.length; i++) {
        if (this.extraMenuItem[i] === 'ordersPage') {
          this.navToOrdersPage();
        }
      }
    }
  }

  logout() {
    this.headerServiceProvider.loading = true
    this.storage.get('token').then(token => {
      this.userServiceProvider.logout(token).subscribe(
        data => {
          this.headerServiceProvider.loading = false
          this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});
          this.storage.remove('token');
        },
        err => {
          this.headerServiceProvider.loading = false
        }
      )
    })
  }
  
  reload() {
    this.navCtrl.setRoot(OrdersPage, {}, {animate: false, direction: 'forward'})
  }

  navToOrdersPage () {
    this.navCtrl.popToRoot()
  }
}
