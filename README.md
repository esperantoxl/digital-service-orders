# Polyonic
## The ultimate "Universal App"

This project combines the <a href="http://electron.atom.io/">Electron Framework</a> with <a href="http://ionicframework.com/docs/v2/">Ionic 2</a> and provides a starter for building out an app that can run on either the desktop (macOS, Windows and Linux), a browser or mobile devices (iOS, Android and Windows Phone).  You can use this application to build and run on one or even all of these platforms.

# Quick start
The dependencies for this project are <a href="https://nodejs.org">Node.js</a>, <a href="http://ionicframework.com/docs/v2/getting-started/installation/">Ionic2 Framework</a> and <a href="https://www.npmjs.com/package/cordova">Cordova</a>.

Make sure you have node installed and running, then install Ionic and Cordova globally using npm.
```
npm install -g ionic cordova
```
Clone the repo, change into the Polyonic directory, install the npm packages and run the Electron app
```
git clone https://github.com/paulsutherland/Polyonic.git
cd Polyonic

npm install
npm start
```
You now have Electron and Ionic running as a Desktop app.

# Running Live Reload for Electron Development
When developing a desktop app, you will want to have the app live reload as you save your changes.  
```
npm run dev
```
# Ionic CLI Commands
When developing a web app, progressive app or native mobile app, all of the Ionic CLI commands are available when you are in the src directory, as this is a standard Ionic app.  Change into the src directory and run commands from the Ionic CLI.  
```
cd src
ionic

eg:
ionic serve --lab --port 4000 -r
```
# Electron packager build
Before building the proxy url should be replaced for the dev url.
`cd src`
`gulp removeProxy`

For production
`cd src`
`gulp addProdUrl`

Build app
`electron-packager ./build app --platform=win32 --overwrite`
